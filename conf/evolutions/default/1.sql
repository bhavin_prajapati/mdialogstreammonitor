# --- First database schema

# --- !Ups

create table user (
  email                     varchar(255) not null primary key,
  name                      varchar(255) not null,
  password                  varchar(255) not null
);

create table stream (
  id                        bigint not null primary key,
  client_name               varchar(255) not null,
  subdomain                 varchar(255) not null,
  api_key                   varchar(255) not null,
  video_asset_ids           varchar(255) not null,
  app_key                   varchar(255) not null
);

create sequence stream_seq start with 1000;

# --- !Downs

drop table if exists user;
drop table if exists stream;
drop sequence if exists stream_seq;
