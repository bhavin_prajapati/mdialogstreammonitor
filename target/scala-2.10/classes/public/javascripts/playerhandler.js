document.body.classList.remove('loading');

var converter = this;

['play', 'pause'].forEach(function (action) {
    document.getElementById(action).addEventListener('click', function () {
        document.body.classList.toggle('paused');
        converter.currentVideo[action]();
    });
});