/* mDialog Javascript SDK */

window.mDialog = (function(){
	var version = '0.1';
    var subdomain, apiKey, appKey;
    var sessionID, deviceID, playerContainer;
    var location, timed_metadata_events_url, stream_time_events_url, low_manifest_url, hd_manifest_url, manifest_url, stream_info_url, loadedStream;
    var live;

	function updateMetadata() {
        var metadata = this.GetTimedMetadataUpdates();
        if (metadata) {
            var eventStr = "";
            for(var objIndex in metadata)
            {
                eventStr += "timedEvent:\n";
                for(var key in metadata[objIndex])
                {
                    eventStr += "  "+key+": "+metadata[objIndex][key]+"\n";
                }
            }
            console.log(eventStr);
        }
    };
	
    function generateToken() {
		var num, string = '', tokens = '0123456789abcdefghiklmnopqrstuvwxyz';
        for (var i = 0; i < 25; i++) {
            num = Math.floor(Math.random() * tokens.length);
            string += tokens.substring(num, num + 1);
        }
        return string;
	};
  
    function getSessionID() {
        var sessionID = sessionStorage.getItem('mdialog-sessionid');
		if (!sessionID) {
			sessionID = generateToken();
			sessionStorage.setItem( 'mdialog-sessionid', sessionID );
		}
		return sessionID;
	};
  
    function getDeviceID() {
		var deviceID = sessionStorage.getItem["mdialog-deviceid"];
		if (!deviceID) {
			deviceID = generateToken();
			sessionStorage.setItem( 'mdialog-deviceid', deviceID );
		}
		return deviceID;
	};
	
	function streamsURLforAssetKey( assetKey ) {
		return "https://" + subdomain + ".mdialog.com/video_assets/" + assetKey + "/streams?device_unique_identifier=" + deviceID + "&z=52522&g=1000004&app=fncipad";
	};
	
	function urlForTemplate(template) {
		var url = template;
		return url.replace("${APPLICATION_KEY}", appKey)
					 	  .replace("${CACHE_BUST}", Math.random().toFixed(8).substr(2))
						  .replace("${SDK}", "javascript")
						  .replace("${SDK_VERSION}", version)
						  .replace("${DEVICE}", window.navigator.platform)
						  .replace("${DEVICE_UNIQUE_ID}", deviceID)
						  .replace("${NETWORK}", "wifi")
						  .replace("${OS_NAME}", "iOS")
						  .replace("${OS_VERSION}", "4.x")
						  .replace("${SESSION_ID}", sessionID)
	};
	
	function pingAnalyticsURLs(urls) {
	    $.each( urls, function() {
	        var analyticsUrl = urlForTemplate(unescape(this['href']));
	        $.get(analyticsUrl);
            console.log("ping -> " + analyticsUrl);
        });
	};
	
    function Stream(streamData) {

        var startURLs = streamData['events']['start']['tracking'];
        var heartBeatURLs = streamData['events']['heartbeat']['tracking'];
        var watchFrequency = 10000;
        var timedMetadataEvents = [];

        function fetchTimedMetadataEvents() {
            $.ajax({
                url: timed_metadata_events_url,
                context: this,
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    timedMetadataEvents = data;
                    console.log(data);
                },
                error: function() { console.log("Timed metadata event update error"); },
            });
        };

        function watchTimedMetadataEvents() {
            this.timedMetadataEventsWatch = setInterval(function(instance){
              fetchTimedMetadataEvents();
            }, watchFrequency, this );
        };

        function pingHeartBeatURL(url) {
            $.ajax({
                type: "GET",
                url: url,
                error: function(xhr, statusText) { console.log("heartbeat error -> "+ statusText); },
                success: function(msg){ console.log('heartbeat -> ' + url ); }
            });
        };

        function watchHeartBeatURL(url, interval) {
            pingHeartBeatURL(url);
            heartBeat = setInterval(function(instance){
                pingHeartBeatURL(url);
            }, (interval * 100), this );
        };

        function initKeepAlive() {
            $.each( heartBeatURLs, function() {
                watchHeartBeatURL(this['href'], this['interval'])
            });
        };

        //initKeepAlive();

        return {
            manifestURL: hd_manifest_url,
            currentTimedMetadataEvents: timedMetadataEvents,

            start: function() {
                console.log('playback started');
                pingAnalyticsURLs(startURLs);
                watchTimedMetadataEvents();
            },
        }
    };
	
	return {

		init: function(options) {
			this.debug = options.debug;
			subdomain = options.subdomain;
			apiKey = options.apiKey;
			appKey = options.appKey;
			assetKey = options.assetKey;
			deviceID = getDeviceID();
			sessionID = getSessionID();
			playerContainer = $(options.playerContainer);
			
			this.error = options.error || function() {
				 console.log("Error updating stream") 
			};
	
			if (this.debug) {
				console.log("mDialog Player Initialized v" + version);
				console.log("-- subdomain: " + subdomain);
				console.log("-- apiKey: " + apiKey);
				console.log("-- appKey: " + appKey);
				console.log("-- assetKey: " + assetKey);
				console.log("-- device ID: " + deviceID);
				console.log("-- session ID: " + sessionID);
			}
		},
		
		appendPlayer: function(url, options) {
			this.player = this.StreamPlayer(url, options);
			playerContainer.append(this.player);
			
			var myMovie = document.getElementById("mDialogLiveStreamPlayer");
			
            if (myMovie) {
                myMovie.addEventListener( "qt_timedmetadataupdated", updateMetadata, false );
                console.log("loadPage: "+myMovie);
            }
		},

		liveStreamPlayer: function(options) {
		    var debug = this.debug;
			this.init(options);
           
            $.ajax({
                url: streamsURLforAssetKey(assetKey),
                context: this,
                type: 'POST',
                dataType: 'json',
                success: function(json, status, xhr) {
                    location = xhr.getResponseHeader("Location");
                    timed_metadata_events_url = json.timed_metadata_events.href;
                    stream_time_events_url = json.stream_time_events.href;
                    low_manifest_url = json.low_manifest.href;
                    hd_manifest_url = json.hd_manifest.href;
                    manifest_url = json.manifest.href;
                    stream_info_url = json.stream_info.href;
                    console.log("Created stream with location: " + location);
                    this.fetchStreamData(stream_info_url);
                },
                error: function(data) { console.log("Stream retrieval error" + data); },
                beforeSend: function(xhr) { xhr.setRequestHeader('Authorization', 'mDialogAPI ' + apiKey); }
            });
		},
		
		fetchStreamData: function(location) {
            $.ajax({
                url: location,
                type: 'GET',
                context: this,
                dataType: 'json',
                success: function(data) {
                    if(data.type == "vod"){
                        live = false;
                        this.appendPlayer(hd_manifest_url);
                    } else {
                        live = true;
                        loadedStream = new Stream(data);
                        this.appendPlayer(loadedStream.manifestURL);
                    }
                },
                error: function() { console.log("Stream data retrieval error"); },
            });
		},

		liveStreamPlayerPlaybackStarted: function() {
		    loadedStream.start();
		}
	}
})();

window.mDialog.StreamPlayer = function(url, options) {
	if (!options) options = {}

//	var player = $('<embed>', {
//		type: "application/x-mpegURL",
//		postdomevents: true,
//		id: "mDialogLiveStreamPlayer"
//	});
//
//	player.attr({
//		src: url,
//		'width': 320,
//		'height': 240,
//	});

    //var player = $('<script>');
    //player.attr({ src: "/assets/javascripts/mpegts/index.js", 'data-canvas': "canvas", 'data-hls': url, 'width': 320, 'height': 240 });


    //Checks if its safari
    var isSafari = false;
    if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
        isSafari = true;
    }

    var player;
    if(isSafari) {
        player = $('<video>', { controls: "controls" });
        player.attr({ src: url, 'width': 320, 'height': 240 });
    } else {
        var flashvars = {
            // M3U8 url, or any other url which compatible with SMP player (flv, mp4, f4m)
            // escaped it for urls with ampersands
            src: escape(url)
            // url to OSMF HLS Plugin
            , plugin_m3u8: "/assets/OSMF/OSMFHLSPlugin.swf"
        };
        var params = {
            // self-explained parameters
            allowFullScreen: true
            , allowScriptAccess: "always"
            , bgcolor: "#000000"
        };
        var attrs = {
            name: "player"
        };

        swfobject.embedSWF(
            // url to SMP player
            "/assets/OSMF/StrobeMediaPlayback.swf",
            // div id where player will be place
            "player",
            // width, height
            "800", "485",
            // minimum flash player version required
            "10.2",
            // other parameters
            null, flashvars, params, attrs
        );
    }
	return swfobject;
};

$('#mDialogLiveStreamPlayer').live('qt_play', function() {
	window.mDialog.liveStreamPlayerPlaybackStarted();
});

// $('#mDialogLiveStreamPlayer').live('qt_timedmetadataupdated', function() {
//   console.log('qt_timedmetadataupdated');
//   console.log(this.GetTimedMetadataUpdates());
//   var metadata = this.GetTimedMetadataUpdates();
//   console.log(metadata)
//   for (index in metadata) {
//     if (metadata[index].key == "TXXX") {
//       if (mDialog.debug) 
//         console.log("Encountered metadata key: " + metadata[index].value);
//       mDialog.timedMetadataEventEncountered(metadata[index].value);
//     }
//   }
// });
