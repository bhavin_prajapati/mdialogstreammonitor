// @SOURCE:/Users/bhavinprajapati/Documents/workspace/mDialogStreamMoniter/conf/routes
// @HASH:1e8f2774aa859f93c877637c2edd5cfe97a9b99a
// @DATE:Fri May 16 01:36:46 EDT 2014

import Routes.{prefix => _prefix, defaultPrefix => _defaultPrefix}
import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._


import Router.queryString


// @LINE:23
// @LINE:20
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:6
package controllers {

// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:6
class ReverseStreams {
    

// @LINE:17
def deleteStream(stream:Long): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "streams" + queryString(List(Some(implicitly[QueryStringBindable[Long]].unbind("stream", stream)))))
}
                                                

// @LINE:15
def getStream(stream:Long): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "stream" + queryString(List(Some(implicitly[QueryStringBindable[Long]].unbind("stream", stream)))))
}
                                                

// @LINE:14
def getStreams(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "streams")
}
                                                

// @LINE:16
def addStream(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "streams")
}
                                                

// @LINE:6
def index(): Call = {
   Call("GET", _prefix)
}
                                                
    
}
                          

// @LINE:23
class ReverseAssets {
    

// @LINE:23
def at(file:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[String]].unbind("file", file))
}
                                                
    
}
                          

// @LINE:20
// @LINE:11
// @LINE:10
// @LINE:9
class ReverseApplication {
    

// @LINE:11
def logout(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "logout")
}
                                                

// @LINE:10
def authenticate(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "login")
}
                                                

// @LINE:20
def javascriptRoutes(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "assets/javascripts/routes")
}
                                                

// @LINE:9
def login(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "login")
}
                                                
    
}
                          
}
                  


// @LINE:23
// @LINE:20
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:6
package controllers.javascript {

// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:6
class ReverseStreams {
    

// @LINE:17
def deleteStream : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Streams.deleteStream",
   """
      function(stream) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "streams" + _qS([(""" + implicitly[QueryStringBindable[Long]].javascriptUnbind + """)("stream", stream)])})
      }
   """
)
                        

// @LINE:15
def getStream : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Streams.getStream",
   """
      function(stream) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "stream" + _qS([(""" + implicitly[QueryStringBindable[Long]].javascriptUnbind + """)("stream", stream)])})
      }
   """
)
                        

// @LINE:14
def getStreams : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Streams.getStreams",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "streams"})
      }
   """
)
                        

// @LINE:16
def addStream : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Streams.addStream",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "streams"})
      }
   """
)
                        

// @LINE:6
def index : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Streams.index",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + """"})
      }
   """
)
                        
    
}
              

// @LINE:23
class ReverseAssets {
    

// @LINE:23
def at : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
   """
)
                        
    
}
              

// @LINE:20
// @LINE:11
// @LINE:10
// @LINE:9
class ReverseApplication {
    

// @LINE:11
def logout : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.logout",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "logout"})
      }
   """
)
                        

// @LINE:10
def authenticate : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.authenticate",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
      }
   """
)
                        

// @LINE:20
def javascriptRoutes : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.javascriptRoutes",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/javascripts/routes"})
      }
   """
)
                        

// @LINE:9
def login : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.login",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
      }
   """
)
                        
    
}
              
}
        


// @LINE:23
// @LINE:20
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:6
package controllers.ref {


// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:6
class ReverseStreams {
    

// @LINE:17
def deleteStream(stream:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Streams.deleteStream(stream), HandlerDef(this, "controllers.Streams", "deleteStream", Seq(classOf[Long]), "DELETE", """""", _prefix + """streams""")
)
                      

// @LINE:15
def getStream(stream:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Streams.getStream(stream), HandlerDef(this, "controllers.Streams", "getStream", Seq(classOf[Long]), "GET", """""", _prefix + """stream""")
)
                      

// @LINE:14
def getStreams(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Streams.getStreams(), HandlerDef(this, "controllers.Streams", "getStreams", Seq(), "GET", """ Streams""", _prefix + """streams""")
)
                      

// @LINE:16
def addStream(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Streams.addStream(), HandlerDef(this, "controllers.Streams", "addStream", Seq(), "POST", """""", _prefix + """streams""")
)
                      

// @LINE:6
def index(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Streams.index(), HandlerDef(this, "controllers.Streams", "index", Seq(), "GET", """ The home page""", _prefix + """""")
)
                      
    
}
                          

// @LINE:23
class ReverseAssets {
    

// @LINE:23
def at(path:String, file:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]), "GET", """ Map static resources from the /public folder to the /public path""", _prefix + """assets/$file<.+>""")
)
                      
    
}
                          

// @LINE:20
// @LINE:11
// @LINE:10
// @LINE:9
class ReverseApplication {
    

// @LINE:11
def logout(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.logout(), HandlerDef(this, "controllers.Application", "logout", Seq(), "GET", """""", _prefix + """logout""")
)
                      

// @LINE:10
def authenticate(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.authenticate(), HandlerDef(this, "controllers.Application", "authenticate", Seq(), "POST", """""", _prefix + """login""")
)
                      

// @LINE:20
def javascriptRoutes(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.javascriptRoutes(), HandlerDef(this, "controllers.Application", "javascriptRoutes", Seq(), "GET", """ Javascript routing""", _prefix + """assets/javascripts/routes""")
)
                      

// @LINE:9
def login(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.login(), HandlerDef(this, "controllers.Application", "login", Seq(), "GET", """ Authentication""", _prefix + """login""")
)
                      
    
}
                          
}
        
    