// @SOURCE:/Users/bhavinprajapati/Documents/workspace/mDialogStreamMoniter/conf/routes
// @HASH:1e8f2774aa859f93c877637c2edd5cfe97a9b99a
// @DATE:Fri May 16 01:36:46 EDT 2014


import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._


import Router.queryString

object Routes extends Router.Routes {

private var _prefix = "/"

def setPrefix(prefix: String) {
  _prefix = prefix
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" }


// @LINE:6
private[this] lazy val controllers_Streams_index0 = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
        

// @LINE:9
private[this] lazy val controllers_Application_login1 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("login"))))
        

// @LINE:10
private[this] lazy val controllers_Application_authenticate2 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("login"))))
        

// @LINE:11
private[this] lazy val controllers_Application_logout3 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("logout"))))
        

// @LINE:14
private[this] lazy val controllers_Streams_getStreams4 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("streams"))))
        

// @LINE:15
private[this] lazy val controllers_Streams_getStream5 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("stream"))))
        

// @LINE:16
private[this] lazy val controllers_Streams_addStream6 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("streams"))))
        

// @LINE:17
private[this] lazy val controllers_Streams_deleteStream7 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("streams"))))
        

// @LINE:20
private[this] lazy val controllers_Application_javascriptRoutes8 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/javascripts/routes"))))
        

// @LINE:23
private[this] lazy val controllers_Assets_at9 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+""",false))))
        
def documentation = List(("""GET""", prefix,"""controllers.Streams.index"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """login""","""controllers.Application.login"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """login""","""controllers.Application.authenticate"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """logout""","""controllers.Application.logout"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """streams""","""controllers.Streams.getStreams"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """stream""","""controllers.Streams.getStream(stream:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """streams""","""controllers.Streams.addStream"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """streams""","""controllers.Streams.deleteStream(stream:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/javascripts/routes""","""controllers.Application.javascriptRoutes"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]] 
}}
      

def routes:PartialFunction[RequestHeader,Handler] = {

// @LINE:6
case controllers_Streams_index0(params) => {
   call { 
        invokeHandler(controllers.Streams.index, HandlerDef(this, "controllers.Streams", "index", Nil,"GET", """ The home page""", Routes.prefix + """"""))
   }
}
        

// @LINE:9
case controllers_Application_login1(params) => {
   call { 
        invokeHandler(controllers.Application.login, HandlerDef(this, "controllers.Application", "login", Nil,"GET", """ Authentication""", Routes.prefix + """login"""))
   }
}
        

// @LINE:10
case controllers_Application_authenticate2(params) => {
   call { 
        invokeHandler(controllers.Application.authenticate, HandlerDef(this, "controllers.Application", "authenticate", Nil,"POST", """""", Routes.prefix + """login"""))
   }
}
        

// @LINE:11
case controllers_Application_logout3(params) => {
   call { 
        invokeHandler(controllers.Application.logout, HandlerDef(this, "controllers.Application", "logout", Nil,"GET", """""", Routes.prefix + """logout"""))
   }
}
        

// @LINE:14
case controllers_Streams_getStreams4(params) => {
   call { 
        invokeHandler(controllers.Streams.getStreams, HandlerDef(this, "controllers.Streams", "getStreams", Nil,"GET", """ Streams""", Routes.prefix + """streams"""))
   }
}
        

// @LINE:15
case controllers_Streams_getStream5(params) => {
   call(params.fromQuery[Long]("stream", None)) { (stream) =>
        invokeHandler(controllers.Streams.getStream(stream), HandlerDef(this, "controllers.Streams", "getStream", Seq(classOf[Long]),"GET", """""", Routes.prefix + """stream"""))
   }
}
        

// @LINE:16
case controllers_Streams_addStream6(params) => {
   call { 
        invokeHandler(controllers.Streams.addStream, HandlerDef(this, "controllers.Streams", "addStream", Nil,"POST", """""", Routes.prefix + """streams"""))
   }
}
        

// @LINE:17
case controllers_Streams_deleteStream7(params) => {
   call(params.fromQuery[Long]("stream", None)) { (stream) =>
        invokeHandler(controllers.Streams.deleteStream(stream), HandlerDef(this, "controllers.Streams", "deleteStream", Seq(classOf[Long]),"DELETE", """""", Routes.prefix + """streams"""))
   }
}
        

// @LINE:20
case controllers_Application_javascriptRoutes8(params) => {
   call { 
        invokeHandler(controllers.Application.javascriptRoutes, HandlerDef(this, "controllers.Application", "javascriptRoutes", Nil,"GET", """ Javascript routing""", Routes.prefix + """assets/javascripts/routes"""))
   }
}
        

// @LINE:23
case controllers_Assets_at9(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        invokeHandler(controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """ Map static resources from the /public folder to the /public path""", Routes.prefix + """assets/$file<.+>"""))
   }
}
        
}

}
     