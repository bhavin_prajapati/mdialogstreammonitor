
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
/**/
object dashboard extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[Seq[Stream],User,Stream,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(streams: Seq[Stream], user: User, stream: Stream ):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.53*/("""

"""),_display_(Seq[Any](/*3.2*/main(streams, user, stream )/*3.30*/{_display_(Seq[Any](format.raw/*3.31*/("""
    
    <header>
        <hgroup>
            <h1>Dashboard</h1>
            <h2>Streams</h2>
        </hgroup>
    </header>
    
    <article  class="tasks">
        """),_display_(Seq[Any](/*13.10*/if(stream == null)/*13.28*/ {_display_(Seq[Any](format.raw/*13.30*/("""
            <p>Please select a stream</p>
        """)))}/*15.11*/else/*15.16*/{_display_(Seq[Any](format.raw/*15.17*/("""
            <p>"""),_display_(Seq[Any](/*16.17*/stream/*16.23*/.subdomain)),format.raw/*16.33*/("""</p>
            <p>"""),_display_(Seq[Any](/*17.17*/stream/*17.23*/.api_key)),format.raw/*17.31*/("""</p>
            """),_display_(Seq[Any](/*18.14*/stream/*18.20*/.video_asset_ids.map/*18.40*/ { video_asset_ids =>_display_(Seq[Any](format.raw/*18.61*/("""
                <p>"""),_display_(Seq[Any](/*19.21*/video_asset_ids)),format.raw/*19.36*/("""</p>
            """)))})),format.raw/*20.14*/("""
            <main>
                <div id="video-streaming"><canvas id="canvas"></canvas></div>
                <br />
                <input class="button orange" type="button" id="play" value="► Play" />
                <input class="button black" type="button" id="pause" value="◼ Pause" />
            </main>

            <div id="video">
            </div>

            <div id="player">
                <!-- this paragraph will be shown if FlashPlayer unavailable -->
                <p>
                    <a href="http://www.adobe.com/go/getflashplayer">
                        <img src="http://wwwimages.adobe.com/www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                    </a>
                </p>
            </div>

            <script type="text/javascript" charset="utf-8">
                $(function()"""),format.raw/*41.29*/("""{"""),format.raw/*41.30*/("""
                    mDialog.liveStreamPlayer("""),format.raw/*42.46*/("""{"""),format.raw/*42.47*/("""
                        subdomain: 'preview-vod',
                        apiKey: 'examplepoc',
                        assetKey: 'haven_test',
                        playerContainer: '#video',
                        debug: true
                    """),format.raw/*48.21*/("""}"""),format.raw/*48.22*/(""");
                """),format.raw/*49.17*/("""}"""),format.raw/*49.18*/(""");
            </script>

        """)))})),format.raw/*52.10*/("""
    </article>
""")))})),format.raw/*54.2*/("""

"""))}
    }
    
    def render(streams:Seq[Stream],user:User,stream:Stream): play.api.templates.HtmlFormat.Appendable = apply(streams,user,stream)
    
    def f:((Seq[Stream],User,Stream) => play.api.templates.HtmlFormat.Appendable) = (streams,user,stream) => apply(streams,user,stream)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu May 22 01:56:13 EDT 2014
                    SOURCE: /Users/bhavinprajapati/Documents/workspace/mDialogStreamMoniter/app/views/dashboard.scala.html
                    HASH: c1e3834b810c553af1ffe6a04e55b20e2ac2f01c
                    MATRIX: 577->1|722->52|759->55|795->83|833->84|1040->255|1067->273|1107->275|1178->328|1191->333|1230->334|1283->351|1298->357|1330->367|1387->388|1402->394|1432->402|1486->420|1501->426|1530->446|1589->467|1646->488|1683->503|1733->521|2642->1402|2671->1403|2745->1449|2774->1450|3054->1702|3083->1703|3130->1722|3159->1723|3226->1758|3274->1775
                    LINES: 19->1|22->1|24->3|24->3|24->3|34->13|34->13|34->13|36->15|36->15|36->15|37->16|37->16|37->16|38->17|38->17|38->17|39->18|39->18|39->18|39->18|40->19|40->19|41->20|62->41|62->41|63->42|63->42|69->48|69->48|70->49|70->49|73->52|75->54
                    -- GENERATED --
                */
            