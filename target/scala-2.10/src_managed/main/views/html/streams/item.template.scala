
package views.html.streams

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
/**/
object item extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[Stream,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(stream: Stream):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.18*/("""

<li data-project=""""),_display_(Seq[Any](/*3.20*/stream/*3.26*/.id)),format.raw/*3.29*/("""">
    <a class="name" href=""""),_display_(Seq[Any](/*4.28*/routes/*4.34*/.Streams.getStream(stream.id.get))),format.raw/*4.67*/("""">"""),_display_(Seq[Any](/*4.70*/stream/*4.76*/.subdomain)),format.raw/*4.86*/("""</a>
    <button formmethod="delete" class="delete" href=""""),_display_(Seq[Any](/*5.55*/routes/*5.61*/.Streams.deleteStream(stream.id.get))),format.raw/*5.97*/("""">Delete</button>
    <span class="loader">Loading</span>
</li>
"""))}
    }
    
    def render(stream:Stream): play.api.templates.HtmlFormat.Appendable = apply(stream)
    
    def f:((Stream) => play.api.templates.HtmlFormat.Appendable) = (stream) => apply(stream)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri May 16 01:44:25 EDT 2014
                    SOURCE: /Users/bhavinprajapati/Documents/workspace/mDialogStreamMoniter/app/views/streams/item.scala.html
                    HASH: 62c8c7b5fdaf14ec0cdd8e7739913c5955bbc631
                    MATRIX: 563->1|673->17|729->38|743->44|767->47|832->77|846->83|900->116|938->119|952->125|983->135|1077->194|1091->200|1148->236
                    LINES: 19->1|22->1|24->3|24->3|24->3|25->4|25->4|25->4|25->4|25->4|25->4|26->5|26->5|26->5
                    -- GENERATED --
                */
            