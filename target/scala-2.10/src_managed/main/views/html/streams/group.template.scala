
package views.html.streams

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
/**/
object group extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[String,Seq[Stream],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(group: String, streams: Seq[Stream] = Nil):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.45*/("""

<li data-group=""""),_display_(Seq[Any](/*3.18*/group)),format.raw/*3.23*/("""">
    <span class="toggle"></span>
    <h4 class="groupName">"""),_display_(Seq[Any](/*5.28*/group)),format.raw/*5.33*/("""</h4>
    <span class="loader">Loading</span>
    <dl class="options">
        <dt>Options</dt>
        <dd>
            <button class="newProject">Add subdomain</button>
            <button class="deleteGroup">Remove account</button>
        </dd>
    </dl>
    <ul>
        """),_display_(Seq[Any](/*15.10*/streams/*15.17*/.map/*15.21*/ { stream =>_display_(Seq[Any](format.raw/*15.33*/("""
            """),_display_(Seq[Any](/*16.14*/views/*16.19*/.html.streams.item(stream))),format.raw/*16.45*/("""
        """)))})),format.raw/*17.10*/("""
    </ul>
</li>
"""))}
    }
    
    def render(group:String,streams:Seq[Stream]): play.api.templates.HtmlFormat.Appendable = apply(group,streams)
    
    def f:((String,Seq[Stream]) => play.api.templates.HtmlFormat.Appendable) = (group,streams) => apply(group,streams)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Fri May 16 00:37:42 EDT 2014
                    SOURCE: /Users/bhavinprajapati/Documents/workspace/mDialogStreamMoniter/app/views/streams/group.scala.html
                    HASH: 72afb11710dba09e82bb455abcad47e8f21d0773
                    MATRIX: 576->1|713->44|767->63|793->68|891->131|917->136|1230->413|1246->420|1259->424|1309->436|1359->450|1373->455|1421->481|1463->491
                    LINES: 19->1|22->1|24->3|24->3|26->5|26->5|36->15|36->15|36->15|36->15|37->16|37->16|37->16|38->17
                    -- GENERATED --
                */
            