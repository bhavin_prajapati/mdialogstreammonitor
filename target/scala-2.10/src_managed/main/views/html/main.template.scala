
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
/**/
object main extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[Seq[Stream],User,Stream,Html,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(streams: Seq[Stream], user: User, stream: Stream)(body: Html):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.64*/("""

<html>
    <head>
        <title>Stream Monitor</title>
        <link rel="shortcut icon" type="image/png" href=""""),_display_(Seq[Any](/*6.59*/routes/*6.65*/.Assets.at("images/favicon.png"))),format.raw/*6.97*/("""">
        <link rel="stylesheet" type="text/css" media="screen" href=""""),_display_(Seq[Any](/*7.70*/routes/*7.76*/.Assets.at("stylesheets/main.css"))),format.raw/*7.110*/("""">
        <script type="text/javascript" src=""""),_display_(Seq[Any](/*8.46*/routes/*8.52*/.Assets.at("javascripts/jquery-1.7.1.js"))),format.raw/*8.93*/(""""></script>
        <script type="text/javascript" src=""""),_display_(Seq[Any](/*9.46*/routes/*9.52*/.Assets.at("javascripts/jquery-play-1.7.1.js"))),format.raw/*9.98*/(""""></script>
        <script type="text/javascript" src=""""),_display_(Seq[Any](/*10.46*/routes/*10.52*/.Assets.at("javascripts/underscore-min.js"))),format.raw/*10.95*/(""""></script>
        <script type="text/javascript" src=""""),_display_(Seq[Any](/*11.46*/routes/*11.52*/.Assets.at("javascripts/backbone-min.js"))),format.raw/*11.93*/(""""></script>
        <script type="text/javascript" src=""""),_display_(Seq[Any](/*12.46*/routes/*12.52*/.Assets.at("javascripts/mdialog.js"))),format.raw/*12.88*/(""""></script>
        <script type="text/javascript" src=""""),_display_(Seq[Any](/*13.46*/routes/*13.52*/.Assets.at("javascripts/main.js"))),format.raw/*13.85*/(""""></script>
        <script type="text/javascript" src="http://yandex.st/swfobject/2.2/swfobject.min.js"></script>
        <script type="text/javascript" src=""""),_display_(Seq[Any](/*15.46*/routes/*15.52*/.Application.javascriptRoutes)),format.raw/*15.81*/(""""></script>
    </head>
    <body>
        <header>
            <a href=""""),_display_(Seq[Any](/*19.23*/routes/*19.29*/.Streams.index)),format.raw/*19.43*/("""" id="logo"><span>Stream</span>Monitor</a>
            <dl id="user">
                <dt>"""),_display_(Seq[Any](/*21.22*/user/*21.26*/.name)),format.raw/*21.31*/(""" <span>("""),_display_(Seq[Any](/*21.40*/user/*21.44*/.email)),format.raw/*21.50*/(""")</span></dt>
                <dd>
                    <a href=""""),_display_(Seq[Any](/*23.31*/routes/*23.37*/.Application.logout())),format.raw/*23.58*/("""">Logout</a>
                </dd>
            </dl>
        </header>
        <nav>
            <h4 class="dashboard"><a href="#/">Dashboard</a></h4>
            <ul id="streams">
                """),_display_(Seq[Any](/*30.18*/streams/*30.25*/.groupBy(_.client_name).map/*30.52*/ {/*31.21*/case (group, streams) =>/*31.45*/ {_display_(Seq[Any](format.raw/*31.47*/("""
                        """),_display_(Seq[Any](/*32.26*/views/*32.31*/.html.streams.group(group, streams))),format.raw/*32.66*/("""
                    """)))}})),format.raw/*34.18*/("""
            </ul>
            <button id="newGroup">Add account</button>
        </nav>
        <section id="main">
            """),_display_(Seq[Any](/*39.14*/body)),format.raw/*39.18*/("""
        </section>
    </body>
</html>

"""))}
    }
    
    def render(streams:Seq[Stream],user:User,stream:Stream,body:Html): play.api.templates.HtmlFormat.Appendable = apply(streams,user,stream)(body)
    
    def f:((Seq[Stream],User,Stream) => (Html) => play.api.templates.HtmlFormat.Appendable) = (streams,user,stream) => (body) => apply(streams,user,stream)(body)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu May 22 01:56:13 EDT 2014
                    SOURCE: /Users/bhavinprajapati/Documents/workspace/mDialogStreamMoniter/app/views/main.scala.html
                    HASH: fd0cea168cee62ad54fbb0a972c8a6896912e158
                    MATRIX: 577->1|733->63|884->179|898->185|951->217|1058->289|1072->295|1128->329|1211->377|1225->383|1287->424|1379->481|1393->487|1460->533|1553->590|1568->596|1633->639|1726->696|1741->702|1804->743|1897->800|1912->806|1970->842|2063->899|2078->905|2133->938|2329->1098|2344->1104|2395->1133|2505->1207|2520->1213|2556->1227|2683->1318|2696->1322|2723->1327|2768->1336|2781->1340|2809->1346|2910->1411|2925->1417|2968->1438|3202->1636|3218->1643|3254->1670|3265->1693|3298->1717|3338->1719|3400->1745|3414->1750|3471->1785|3526->1825|3692->1955|3718->1959
                    LINES: 19->1|22->1|27->6|27->6|27->6|28->7|28->7|28->7|29->8|29->8|29->8|30->9|30->9|30->9|31->10|31->10|31->10|32->11|32->11|32->11|33->12|33->12|33->12|34->13|34->13|34->13|36->15|36->15|36->15|40->19|40->19|40->19|42->21|42->21|42->21|42->21|42->21|42->21|44->23|44->23|44->23|51->30|51->30|51->30|51->31|51->31|51->31|52->32|52->32|52->32|53->34|58->39|58->39
                    -- GENERATED --
                */
            