import akka.actor.FSM.->
import anorm.Useful.Var
import play.api._

import models._
import anorm._


object Global extends GlobalSettings {
  
  override def onStart(app: Application) {
    InitialData.insert()
  }
  
}

/**
 * Initial set of data to be imported 
 * in the sample application.
 */
object InitialData {

  def date(str: String) = new java.text.SimpleDateFormat("yyyy-MM-dd").parse(str)

  def insert() = {

    if(User.findAll.isEmpty) {

      Seq(
        User("bprajapati@mdialog.com", "Bhavin Prajapati", "password"),
        User("cpatel@mdialog.com", "Chirayu Patel", "password")
      ).foreach(User.create)

      Seq(
        Stream(Id(1), "CBS", "cbs-vod" , "eb09d44d00cf0769508dd230eea0c6da", List("7FpMLhrybyJgKJysggbhtnVAJIOFTz_s", "U57kFIzh6OPRv_tITOs7IbTFQR6W1hdG", "zJSxds5wRhmga7PW2vnZD6ZfzlhCasex"), "streammonitor"),
        Stream(Id(2), "Fox News", "foxnews" , "b68dfe81f017b133bed6ea37972417d2", List("2608761480001"), "streammonitor"),
        Stream(Id(3), "Fox News", "foxnews-tve" , "b68dfe81f017b133bed6ea37972417d2", List("1241186546001"), "streammonitor"),
        Stream(Id(4), "NBC Universal", "nbcui" , "838d0cce9ca15f03d6c4725252b28f45", List("nbcuni-snf"), "streammonitor"),
        Stream(Id(5), "A&E", "aetn-vod" , "599b88193e3f5a6c96f7a362a75b34ad", List("132526659865", "139869251947", "135989827606", "25181251553", "29059139958"), "streammonitor"),
        Stream(Id(6), "WSJ", "wsj" , "8249ce5028c720bc33f952476d80c454", List("5d015cbc-9755-11e0-b515-1231393309f6"), "streammonitor")
      ).foreach {
        case (stream) => Stream.create(stream)
      }

    }

  }

}