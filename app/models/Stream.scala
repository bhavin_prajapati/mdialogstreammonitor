package models

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import anorm.~
import play.api.Play.current

case class Stream(id: Pk[Long], client_name: String, subdomain: String, api_key: String, video_asset_ids: List[String], app_key: String)

object Stream {

  // -- Parsers

  /**
   * Parse a Stream from a ResultSet
   */
  val simple = {
    get[Pk[Long]]("stream.id") ~
    get[String]("stream.client_name") ~
    get[String]("stream.subdomain") ~
    get[String]("stream.api_key") ~
    get[String]("stream.video_asset_ids") ~
    get[String]("stream.app_key") map {
      case id~client_name~subdomain~api_key~video_asset_ids~app_key => Stream(id, client_name, subdomain, api_key, video_asset_ids.split(";").toList, app_key)
    }
  }

  // -- Queries

  /**
   * Retrieve a Stream from id.
   */
  def findById(id: Long): Option[Stream] = {
    DB.withConnection { implicit connection =>
      SQL("select * from stream where id = {id}").on(
        'id -> id
      ).as(Stream.simple.singleOpt)
    }
  }

  /**
   * Retrieve all streams
   */
  def getAllStreams(): Seq[Stream] = {
    DB.withConnection { implicit connection =>
      SQL(
        """
          select * from stream
        """
      ).as(Stream.simple *)
    }
  }

  /**
   * Delete a stream.
   */
  def delete(id: Long) {
    DB.withConnection { implicit connection =>
      SQL("delete from stream where id = {id}").on(
        'id -> id
      ).executeUpdate()
    }
  }

  /**
   * Create a stream.
   */
  def create(stream: Stream): Stream = {
    DB.withTransaction { implicit connection =>

    // Get the stream id
      val id: Long = stream.id.getOrElse {
        SQL("select next value for stream_seq").as(scalar[Long].single)
      }

      // Insert the stream
      SQL(
        """
           insert into stream values (
             {id}, {client_name}, {subdomain}, {api_key}, {video_asset_ids}, {app_key}
           )
        """
      ).on(
          'id -> id,
          'client_name -> stream.client_name,
          'subdomain -> stream.subdomain,
          'api_key -> stream.api_key,
          'video_asset_ids -> (stream.video_asset_ids mkString (";")),
          'app_key -> stream.app_key
        ).executeUpdate()

      stream.copy(id = Id(id))

    }
  }
}
