package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

import anorm._

import models._
import views._

/**
 * Manage projects related operations.
 */
object Streams extends Controller with Secured {

  /**
   * Display the dashboard.
   */
  def index = IsAuthenticated { username => _ =>
    User.findByEmail(username).map { user =>
      Ok(
        html.dashboard(Stream.getAllStreams(), user, null )
      )
    }.getOrElse(Forbidden)
  }

  /**
   * Display the tasks panel for this project.
   */
  def index(stream: Long) = IsAuthenticated { _ => implicit request =>
    Stream.findById(stream).map { p =>
      Ok(html.streams.index(p))
    }.getOrElse(NotFound)
  }

  // -- Projects

  /**
   * Gets all streams
   */
  def getStreams = IsAuthenticated { _ => implicit request =>
    Stream.getAllStreams()
    Ok
  }

  /**
   * Gets a stream
   */
  def getStream(id: Long) = IsAuthenticated { username => _ =>
    User.findByEmail(username).map { user =>
      Stream.findById(id).map { p =>
        Ok(html.dashboard(Stream.getAllStreams(), user , p ))
      }.getOrElse(NotFound)
    }.getOrElse(Forbidden)
  }

  /**
   * Add a stream.
   */
  def addStream = IsAuthenticated { username => implicit request =>
    Form("group" -> nonEmptyText).bindFromRequest.fold(
      errors => BadRequest,
      folder => Ok(
        views.html.streams.item(
          Stream.create(
            Stream(NotAssigned, "client_name", "subdomain" , "api_key", List("video_asset_ids"), "app_key")
          )
        )
      )
    )
  }

  /**
   * Delete a stream.
   */
  def deleteStream(stream: Long) = IsAuthenticated { _ => _ =>
    Stream.delete(stream)
    Ok
  }
}

